## Running migrations on prod

- To get database url: `gigalixir configs hilarious-delirious-fallowdeer`
- In order to run migrations, hook up directly to that beast: `MIX_ENV=prod DATABASE_URL="the url of prod db" mix ecto.migrate`
