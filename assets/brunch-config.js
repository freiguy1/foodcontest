exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {
      joinTo: {
        "js/app.js": /^(?!js\/pages)/, // Will not grab anything from js/pages
        "js/entry.js": /^js\/pages\/entry/, // Only grabs the entry jses
        "js/competition.js": /^js\/pages\/competition/ // Only grabs the entry jses
      },
    },
    stylesheets: {
      joinTo: {
        "css/app.css": /^(?!css\/pages)/, // Will not grab anything from css/pages
        "css/index.css": /^css\/pages\/index/, // Only grabs the index css
      },
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to "/assets/static". Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(static)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: ["static", "css", "js", "vendor"],
    // Where to compile files to
    public: "../priv/static"
  },

  // Configure your plugins
  plugins: {
    babel: {
      // Do not use ES6 compiler in vendor code
      ignore: [/vendor/]
    },
    sass: {
      options: {
        includePaths: ["node_modules/bootstrap-sass/assets/stylesheets"], // tell sass-brunch where to look for files to @import
        precision: 8 // minimum precision required by bootstrap-sass 
      }
    },
    copycat: {
      "fonts/bootstrap": ["node_modules/bootstrap-sass/assets/fonts/bootstrap"] // copy node_modules/bootstrap-sass/assets/fonts/bootstrap/* to priv/static/fonts/
    }
  },

  modules: {
    autoRequire: {
      "js/app.js": ["js/app"],
      "js/entry.js": ["js/pages/entry/entry"],
      "js/competition.js": ["js/pages/competition/competition"]
    }
  },

  npm: {
    enabled: true,
    globals: {
      $: 'jquery',
      jQuery: 'jquery',
      moment: 'moment',
      bootstrap: 'bootstrap-sass'
    },
  }
};
