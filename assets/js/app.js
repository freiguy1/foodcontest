// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"


import "phoenix_html"
// var $ = require('jquery');
// var moment = require('moment');

$(function() {
  $('.localize-me').each(function() {
    var data = $(this).data('datetime');
    var jsDate = new Date(data);
    var mDate = moment(jsDate);
    var formatted = mDate.calendar();
    $(this).html(formatted);
  });

  $('input.localize-value').each(function() {
    var data = $(this).attr('value');
    if (!data) return;
    var jsDate = new Date(data);
    var mDate = moment(jsDate);
    var formatted = mDate.format('YYYY-MM-DDTHH:mm');
    $(this).attr('value', formatted);
  });

  $('.duration-me').each(function() {
    var startData = $(this).data('start-datetime');
    var endData = $(this).data('end-datetime');
    var startDate = new Date(startData);
    var endDate = new Date(endData);
    var mStartDate = moment(startDate);
    var mEndDate = moment(endDate);
    var duration = moment.duration(mEndDate - mStartDate);
    var days = duration.days();
    var hours = duration.hours();
    var minutes = duration.minutes();
    var formatted = "";
    if (days == 1) formatted += "1 day ";
    else if (days !== 0) formatted += days + " days ";
    if (hours == 1) formatted += "1 hour ";
    else if (hours !== 0) formatted += hours + " hours ";
    if (minutes == 1) formatted += "1 minute";
    else if (minutes !== 0) formatted += minutes + " minutes";
    $(this).html(formatted);
  });

  $('input.convert-utc').change(convertInputToUtc);
});

function convertInputToUtc() {
  var element = $(this);
  var targetId = element.data('local-for');
  if (!targetId) return;
  var dateTimeValue = moment(element.val());
  if (!dateTimeValue) return;
  $('#' + targetId).val(dateTimeValue.utc().format());
}
