$(function() {
  $('.entry-list-item .vote').click(onVoteClicked);
  $('#vote-modal button').click(onSubmitVoteClicked);
});

function onVoteClicked() {
  var entryId = $(this).data('entry-id');
  var entryName = $(this).data('entry-name');
  $('#voter-name').val('');
  $('#voter-comments').val('');
  $('#vote-modal #entry-id').val(entryId);
  $('#vote-modal .entry-id').html(entryId);
  $('#vote-modal .entry-name').html(entryName);
  $('#vote-modal #confirmation').hide();
  $('#vote-modal #vote-form').show();
  $('#vote-modal').modal();
}

function onSubmitVoteClicked() {
  var name = $('#voter-name').val();
  var comments = $('#voter-comments').val();
  var entryId = $('#vote-modal #entry-id').val();
  var data = {
    vote: {
      voter_name: name,
      comment: comments
    }
  };
  $.post("/api/votes/" + entryId, data, onSubmitVoteSuccess, "json")
  .fail(function() {
    alert("something didn't work");
  });
}

function onSubmitVoteSuccess() {
  $('#vote-modal #confirmation').show();
  $('#vote-modal #vote-form').hide();
}
