# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :cookoff, Cookoff.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "cookoff_repo",
  username: "user",
  password: "pass",
  hostname: "localhost"


config :cookoff, Cookoff.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "cookoff_repo",
  username: "user",
  password: "pass",
  hostname: "localhost"


# General application configuration
config :cookoff,
  ecto_repos: [Cookoff.Repo]

# Configures the endpoint
config :cookoff, CookoffWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SeiHyL+UHKqwaToFMDr4RunHhiQ5CdCUo3oiz73dXc/xbfowWlNqCzGvFwM8KHzn",
  render_errors: [view: CookoffWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Cookoff.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
