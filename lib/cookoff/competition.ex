defmodule Cookoff.Competition do
  use Ecto.Schema
  import Ecto.Changeset
  alias Cookoff.Competition


  schema "competitions" do
    field :description, :string
    field :facilitator_email, :string
    field :facilitator_first_name, :string
    field :facilitator_last_name, :string
    field :vote_start_time_local, :naive_datetime
    field :vote_end_time_local, :naive_datetime
    field :name, :string
    field :private_key, Ecto.UUID
    field :public_key, Ecto.UUID
    field :vote_end_time_utc, :utc_datetime
    field :vote_start_time_utc, :utc_datetime
    has_many :entries, Cookoff.Entry

    timestamps()
  end

  @doc false
  def changeset(%Competition{} = competition, attrs) do
    competition
    |> cast(attrs, [:name, :private_key, :public_key, :vote_start_time_utc, :vote_end_time_utc, :vote_start_time_local, :vote_end_time_local, :description, :facilitator_first_name, :facilitator_last_name, :facilitator_email])
    |> validate_required([:name, :private_key, :public_key, :vote_start_time_utc, :vote_end_time_utc, :vote_start_time_local, :vote_end_time_local, :facilitator_first_name, :facilitator_last_name, :facilitator_email])
  end
end
