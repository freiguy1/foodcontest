defmodule Cookoff.Entry do
  use Ecto.Schema
  import Ecto.Changeset
  alias Cookoff.Entry


  schema "entries" do
    field :chef_name, :string
    field :description, :string
    field :name, :string
    field :number, :integer
    field :private_key, Ecto.UUID
    belongs_to :competition, Cookoff.Competition
    has_many :votes, Cookoff.Vote

    timestamps()
  end

  @doc false
  def changeset(%Entry{} = entry, attrs) do
    entry
    |> cast(attrs, [:name, :chef_name, :description, :number, :private_key, :competition_id])
    |> validate_required([:name, :chef_name, :number, :private_key, :competition_id])
  end
end
