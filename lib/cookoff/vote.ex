defmodule Cookoff.Vote do
  use Ecto.Schema
  import Ecto.Changeset
  alias Cookoff.Vote


  schema "votes" do
    field :comment, :string
    field :voter_name, :string
    belongs_to :entry, Cookoff.Entry

    timestamps()
  end

  @doc false
  def changeset(%Vote{} = vote, attrs) do
    vote
    |> cast(attrs, [:voter_name, :comment, :entry_id])
    |> validate_required([:entry_id])
  end
end
