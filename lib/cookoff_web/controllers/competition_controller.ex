defmodule CookoffWeb.CompetitionController do
  use CookoffWeb, :controller
  alias Cookoff.{Repo, Competition}
  import Ecto.Query, only: [from: 2]

  plug :valid_uuid, "key" when action in [:public, :manage, :update, :update_put]

  def public(conn, %{"key" => key}) do
    case Competition |> Repo.get_by(public_key: key) |> Repo.preload(:entries) do
      nil -> conn |> put_status(:not_found) |> text("not found")
      competition -> render conn, :public, %{competition: competition}
    end
  end

  def manage(conn, %{"key" => key}) do
    competition = Repo.one from competition in Competition,
      where: competition.private_key == ^key,
      left_join: entries in assoc(competition, :entries),
      left_join: votes in assoc(entries, :votes),
      preload: [entries: {entries, votes: votes}]
    #case Competition |> Repo.get_by(private_key: key) |> Repo.preload(:entries) |> Repo.preload(:votes) do
    case competition do
      nil -> conn |> put_status(:not_found) |> text("not found")
      competition -> render conn, :manage, %{competition: competition}
    end
  end

  def update(conn, %{errors: errors}) do
    render conn, :update, changeset: errors
  end

  def update(conn, %{"key" => key}) do
    case Competition |> Repo.get_by(private_key: key) do
      nil -> conn |> put_status(:not_found) |> text("not found")
      competition -> render conn, :update, changeset: Competition.changeset(competition, %{})
    end
  end

  def update_put(conn, %{"key" => key, "competition" => updated_competition}) do
    updated_competition = updated_competition
    |> Map.update!("vote_start_time_local", fn x -> x <> ":00" end)
    |> Map.update!("vote_end_time_local", fn x -> x <> ":00" end)

    case Competition |> Repo.get_by(private_key: key) do
      nil -> conn |> put_status(:not_found) |> text("not found")
      competition ->
        case Competition.changeset(competition, updated_competition) |> Repo.update do
          {:ok, %{private_key: private_key}} -> redirect conn, to: competition_path(conn, :manage, private_key)
          {:error, reasons} -> update conn, %{errors: reasons}
        end
    end
  end

  def new(conn, %{errors: errors}) do
    render conn, :new, changeset: errors
  end

  def new(conn, _) do
    changeset = Competition.changeset(%Competition{}, %{})
    render conn, :new, changeset: changeset
  end

  def create(conn, %{"competition" => competition}) do
    competition = competition
    |> Map.update!("vote_start_time_local", fn x -> x <> ":00" end)
    |> Map.update!("vote_end_time_local", fn x -> x <> ":00" end)
    |> Map.put("private_key", Ecto.UUID.generate())
    |> Map.put("public_key", Ecto.UUID.generate())

    changeset = Competition.changeset(%Competition{}, competition)

    case Repo.insert changeset do
      {:ok, %{private_key: private_key}} -> redirect conn, to: competition_path(conn, :manage, private_key)
      {:error, reasons} ->
        new conn, %{errors: reasons}
    end
  end

  def results(conn, %{"key" => key}) do
    competition = Repo.one from competition in Competition,
      where: competition.public_key == ^key,
      left_join: entries in assoc(competition, :entries),
      left_join: votes in assoc(entries, :votes),
      preload: [entries: {entries, votes: votes}]
    case competition do
      nil -> conn |> put_status(:not_found) |> text("not found")
      competition -> render conn, :results, competition: competition
    end
  end

  ## PRIVATE PARTS BELOW

  defp valid_uuid(conn, key_name) do
    case conn.params[key_name] |> Ecto.UUID.cast do
      :error -> conn |> put_status(:not_found) |> text("not found") |> halt()
      _ -> conn
    end
  end

end
