defmodule CookoffWeb.EntryController do
  use CookoffWeb, :controller
  alias Cookoff.{Repo, Entry, Competition}

  plug :valid_uuid, "key" when action in [:manage, :update, :withdraw]

  def manage(conn, %{errors: errors}) do
    full_entry = errors.data |> Repo.preload(:competition)
    errors = %{errors | data: full_entry}
    render conn, :manage, changeset: errors
  end

  def manage(conn, %{"key" => key}) do
    case Entry |> Repo.get_by(private_key: key) do
      nil -> conn |> put_status(:not_found) |> text("not found")
      entry ->
        entry = entry |> Repo.preload(:competition) 
        render conn, :manage, changeset: Entry.changeset(entry, %{})
    end
  end

  def update(conn, %{"key" => key, "entry" => updated_entry}) do
    case Entry |> Repo.get_by(private_key: key) do
      nil -> conn |> put_status(:not_found) |> text("not found")
      entry ->
        case Entry.changeset(entry, updated_entry) |> Repo.update do
          {:ok, entry} -> 
            entry = entry |> Repo.preload(:competition)
            render conn, :manage, changeset: Entry.changeset(entry, %{})
          {:error, reasons} -> manage conn, %{errors: reasons}
        end
    end
  end

  def new(conn, %{errors: errors}) do
    render conn, :new, changeset: errors
  end

  def new(conn, _) do
    changeset = Entry.changeset(%Entry{}, %{})
    render conn, :new, changeset: changeset
  end

  def create(conn, %{"entry" => entry, "key" => key}) do
    case Competition |> Repo.get_by(public_key: key) do
      nil -> conn |> put_status(:not_found) |> text("not found")
      competition -> 
        number = get_next_entry_number(competition.id)
        entry = Map.put(entry, "competition_id", competition.id)
        entry = Map.put(entry, "number", number)
        entry = Map.put(entry, "private_key", Ecto.UUID.generate())
        changeset = Entry.changeset(%Entry{}, entry)
        case Repo.insert changeset do
          {:ok, %{private_key: private_key}} -> redirect conn, to: entry_path(conn, :manage, private_key)
          {:error, reasons} -> new conn, %{errors: reasons}
        end
    end
  end

  # competition management delete action
  def delete(conn, %{"entry_private_key" => entry_key, "competition_private_key" => competition_key}) do
    on_success = fn() ->
      redirect conn, to: competition_path(conn, :manage, competition_key)
    end
    case Competition |> Repo.get_by(private_key: competition_key) do
      nil -> conn |> put_status(:not_found) |> text("not found")
      competition ->
        competition = competition |> Repo.preload(:entries)
        entry = Enum.find(competition.entries, &(&1.private_key == entry_key))
        delete_entry(conn, entry, on_success)
    end
  end

  # entry management withdraw action
  def withdraw(conn, %{"key" => entry_key}) do
    case Entry |> Repo.get_by(private_key: entry_key) |> Repo.preload(:competition) do
      nil -> conn |> put_status(:not_found) |> text("not found")
      entry ->
        on_success = fn() ->
          redirect conn, to: competition_path(conn, :public, entry.competition.public_key)
        end
        delete_entry(conn, entry, on_success)
    end
  end

  ## PRIVATE PARTS BELOW

  defp delete_entry(conn, nil, _) do conn |> put_status(:not_found) |> text("not found") end

  defp delete_entry(_conn, entry, on_success) do 
    Repo.delete!(entry)
    on_success.()
  end

  defp get_next_entry_number(competition_id) do
    numbers = Entry
    |> Repo.all(competition_id: competition_id)
    |> Enum.map(fn e -> e.number end)
    |> Enum.sort
    |> MapSet.new
    range = 1..(Enum.count(numbers) + 1)
    |> MapSet.new
    MapSet.difference(range, numbers)
    |> MapSet.to_list
    |> List.first
  end

  defp valid_uuid(conn, key_name) do
    case conn.params[key_name] |> Ecto.UUID.cast do
      :error -> conn |> put_status(:not_found) |> text("not found")
      _ -> conn
    end
  end

end


