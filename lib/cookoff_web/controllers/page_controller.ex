defmodule CookoffWeb.PageController do
  use CookoffWeb, :controller

  # plug :put_layout, :index

  def index(conn, _params) do
    render conn, "index.html", layout: {CookoffWeb.LayoutView, :index}
  end
end
