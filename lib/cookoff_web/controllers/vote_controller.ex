defmodule CookoffWeb.VoteController do
  use CookoffWeb, :controller
  alias Cookoff.{Repo, Entry, Vote}

  def create(conn, %{"entry_id" => entry_id, "vote" => vote}) do
    case Entry |> Repo.get(entry_id) |> Repo.preload(:competition) do
      nil -> conn |> put_status(:not_found) |> text("{\"error\": \"not found\"}")
      entry ->
        if is_voting_open?(entry.competition) do
          try_create_vote(conn, entry_id, vote)
        else
          conn |> put_status(:bad_request) |> text("{\"error\": \"not valid voting time, friend\"}")
        end
    end
  end

  ## PRIVATE PARTS BELOW

  defp is_voting_open?(competition) do
    now = DateTime.utc_now()
    DateTime.compare(now, competition.vote_start_time) == :gt &&
      DateTime.compare(now, competition.vote_end_time) == :lt
  end

  defp try_create_vote(conn, entry_id, vote) do
    vote = Map.put(vote, "entry_id", entry_id)
    changeset = Vote.changeset(%Vote{}, vote)
    case Repo.insert changeset do
      {:ok, _} -> conn |> text("{\"success\": true}")
      {:error, _} -> conn |> put_status(:error) |> text("{\"error\": \"something bad happened\"}")
    end
  end
end

