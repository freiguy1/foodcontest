defmodule CookoffWeb.Router do
  use CookoffWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", CookoffWeb do
    pipe_through :api
    scope "/votes" do
      post "/:entry_id", VoteController, :create
    end
  end

  scope "/", CookoffWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    scope "/competitions" do
      get "/:key/new_entry", EntryController, :new
      post "/:key/new_entry", EntryController, :create
      get "/manage/:competition_private_key/delete_entry/:entry_private_key", EntryController, :delete

      get "/new", CompetitionController, :new
      post "/new", CompetitionController, :create
      get "/update/:key", CompetitionController, :update
      put "/update/:key", CompetitionController, :update_put
      get "/manage/:key", CompetitionController, :manage
      get "/:key", CompetitionController, :public
      get "/:key/results", CompetitionController, :results
    end

    scope "/entries" do
      get "/:key", EntryController, :manage
      put "/:key", EntryController, :update
      get "/delete/:key", EntryController, :withdraw
    end

  end

  # Other scopes may use custom stacks.
  # scope "/api", CookoffWeb do
  #   pipe_through :api
  # end
end
