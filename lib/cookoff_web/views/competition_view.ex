defmodule CookoffWeb.CompetitionView do
  use CookoffWeb, :view

  def format_name(first, last) do
    "#{first} #{last}"
  end

  def current_date_string() do
    date = Ecto.Date.utc() |> Ecto.Date.to_iso8601
    "#{date}T00:00:00"
  end

  def render("includes.js", assigns) do
    competition_path = static_path(assigns.conn, "/js/competition.js")
    raw("<script src=\"#{competition_path}\"></script>")
  end

  def format_datetime(nil), do: ""
  def format_datetime(dt) when is_binary(dt), do: dt |> String.slice(0..15) # remove seconds if string
  def format_datetime(dt) do
    IO.inspect(dt)
    NaiveDateTime.to_iso8601(dt)
    |> String.slice(0..15)
  end

  def title(:manage, assigns), do: "Manage Competition - #{assigns.competition.name}"
  def title(:update, _), do: "Update Competition"
  def title(:new, _), do: "New Competition"
  def title(_, _), do: "Food Contest"

end
