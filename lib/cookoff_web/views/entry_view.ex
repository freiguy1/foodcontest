defmodule CookoffWeb.EntryView do
  use CookoffWeb, :view

  def format_title(number, name) do
    "##{number} - #{name}"
  end

  def render("includes.js", assigns) do
    entry_path = static_path(assigns.conn, "/js/entry.js")
    raw("<script src=\"#{entry_path}\"></script>")
  end

end
