defmodule CookoffWeb.LayoutView do
  use CookoffWeb, :view

  def title(conn, assigns, default) do
    try do 
      apply(view_module(conn), :title, [action_name(conn), assigns])
    rescue
      _ ->
        default
    end
  end

end
