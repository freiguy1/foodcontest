defmodule Cookoff.Repo.Migrations.CreateCompetitions do
  use Ecto.Migration

  def change do
    create table(:competitions) do
      add :name, :string
      add :private_key, :uuid
      add :public_key, :uuid
      add :vote_start_time_utc, :utc_datetime
      add :vote_end_time_utc, :utc_datetime
      add :vote_start_time_local, :naive_datetime
      add :vote_end_time_local, :naive_datetime
      add :description, :text
      add :facilitator_first_name, :string
      add :facilitator_last_name, :string
      add :facilitator_email, :string

      timestamps()
    end

  end
end
