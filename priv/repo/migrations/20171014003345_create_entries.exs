defmodule Cookoff.Repo.Migrations.CreateEntries do
  use Ecto.Migration

  def change do
    create table(:entries) do
      add :name, :string
      add :chef_name, :string
      add :description, :text
      add :number, :integer
      add :private_key, :uuid
      add :competition_id, references(:competitions)

      timestamps()
    end

  end
end
