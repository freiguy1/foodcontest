defmodule Cookoff.Repo.Migrations.CreateVotes do
  use Ecto.Migration

  def change do
    create table(:votes) do
      add :voter_name, :string
      add :comment, :string
      add :entry_id, references(:entries)

      timestamps()
    end

  end
end
