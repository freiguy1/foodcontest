# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Cookoff.Repo.insert!(%Cookoff.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# changeset = Competition.changeset(%Competition{}, %{name: "First Competition", private_key: Ecto.UUID.generate(), public_key: Ecto.UUID.generate, vote_start_time: "2018-03-02 12:00:00", vote_end_time: "2018-03-02 15:00:00", facilitator_first_name: "Joe", facilitator_last_name: "Bob", facilitator_email: "123@thing.com"})
